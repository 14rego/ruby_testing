# File.open("text.txt").each { |line| puts line }

#text=''
#line_count = 0
#File.open("text.txt").each { |line| 
#  line_count += 1
#  text << line
#}

lines = File.readlines("text.txt")
line_count = lines.size
text = lines.join
total_chars = text.length
total_content = text.gsub(/\s+/, "").length
word_count = text.split.length
sentence_count = text.split(/\.|\?|\!/).length
para_count = text.split(/\n\n/).length

puts "Line count: #{line_count}."
puts "Total Characters: #{total_chars}."
puts "Total without spaces: #{total_content}."
puts "Total words: #{word_count}."
puts "Total sentences: #{sentence_count}."
puts "Total paragraphs: #{para_count}."

puts "Avarage sentences per paragraph: #{sentence_count / para_count}."
puts "Average words per sentence: #{word_count / sentence_count}."
